package com.plbtw.plbtw_admin.Hadiah;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.plbtw.plbtw_admin.JSONParser;
import com.plbtw.plbtw_admin.R;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class EditHadiahActivity extends AppCompatActivity {

    private ImageView imageView;
    private Bitmap bitmap;
    private int PICK_IMAGE_REQUEST = 1;
    private String UPLOAD_IMAGE_URL ="http://pinjemindong.esy.es/plbtw/hadiah/update_image.php";
    private String URL_UPDATE="http://pinjemindong.esy.es/plbtw/hadiah/update.php";
    private String KEY_IMAGE = "image";

    EditText txtNamaHadiah,txtJumlah,txtPoin;
    Button btnChose,btnTambah;

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_hadiah);

        btnChose = (Button) findViewById(R.id.buttonChoose);
        btnTambah= (Button) findViewById(R.id.btnEdit);
        imageView  = (ImageView) findViewById(R.id.imageView);
        txtJumlah = (EditText) findViewById(R.id.txtJumlah);
        txtNamaHadiah = (EditText) findViewById(R.id.txtNamaHadiah);
        txtPoin = (EditText) findViewById(R.id.txtPoin);

        Bundle b=getIntent().getExtras();
        final String id_hadiah=b.getString("id_hadiah");
        txtJumlah.setText(b.getString("jumlah"));
        txtNamaHadiah.setText(b.getString("nama_hadiah"));
        txtPoin.setText(b.getString("poin"));

        String url="http://pinjemindong.esy.es/plbtw/image/"+ id_hadiah+".png";

        //Toast.makeText(this, url, Toast.LENGTH_LONG).show();
        Picasso.with(EditHadiahActivity.this).load(url).into(imageView);

        bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        btnChose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkTextbox()) {
                    new EditHadiahJSON(id_hadiah,txtNamaHadiah.getText().toString(),txtPoin.getText().toString(),txtJumlah.getText().toString()).execute();
                    uploadImage(id_hadiah);
                }
            }
        });
    }


    private boolean checkTextbox(){
        boolean temp=true;
        if(txtJumlah.getText().toString().isEmpty()){
            txtJumlah.setError("Jumlah tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtJumlah.setError(null);
        }
        if(txtNamaHadiah.getText().toString().isEmpty()){
            txtNamaHadiah.setError("Nama hadiah tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtNamaHadiah.setError(null);
        }
        if(txtPoin.getText().toString().isEmpty()){
            txtPoin.setError("Poin tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtPoin.setError(null);
        }

        return temp;
    }


    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void uploadImage(String id_hadiah){
        //Showing the progress dialog
        final String temp_id_hadiah=id_hadiah;
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_IMAGE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        //loading.dismiss();
                        //Showing toast message of the response
                        Toast.makeText(EditHadiahActivity.this, s, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();

                        //Showing toast
                        Toast.makeText(EditHadiahActivity.this, volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();
                //String username=sharedpreferences.getString("username","");

                //Adding parameters
                params.put(KEY_IMAGE, image);
                params.put("id_hadiah", temp_id_hadiah);

                //params.put("username", username);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //JSON kelas untuk login
    class EditHadiahJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String nama_hadiah,poin,jumlah,id_hadiah;

        public EditHadiahJSON(String id_hadiah,String nama_hadiah,String poin, String jumlah){
            this.id_hadiah=id_hadiah;
            this.nama_hadiah=nama_hadiah;
            this.poin=poin;
            this.jumlah=jumlah;
        }

        protected  void onPreExecute(){
            super.onPreExecute();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_hadiah",id_hadiah));
            params.add(new BasicNameValuePair("nama_hadiah",nama_hadiah));
            params.add(new BasicNameValuePair("poin",poin));
            params.add(new BasicNameValuePair("jumlah",jumlah));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_UPDATE,"GET",params);
                Log.d("", json.toString());

                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){
                            Log.d("", json.toString());
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){
            if(sukses.equalsIgnoreCase("berhasil")) {
                Toast.makeText(EditHadiahActivity.this, "Berhasil diubah :D", Toast.LENGTH_SHORT).show();
                finish();
            }
            else {
                Toast.makeText(EditHadiahActivity.this, "Error :(", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
