package com.plbtw.plbtw_admin.Bencana;

import android.os.AsyncTask;
import android.util.Log;

import com.plbtw.plbtw_admin.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kevin on 23/05/2016.
 */
public class TopupPoin {
    private static final String URL_READ="http://pinjemindong.esy.es/plbtw/pengguna/topup_poin.php";
    ArrayList<HashMap<String, String>> arrayListBencana;

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";


    public TopupPoin(String username,String poin){
        new TopupPoinJSON(username,poin).execute();
    }

    class TopupPoinJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String username,poin;

        public TopupPoinJSON(String username,String poin){
            this.username=username;
            this.poin=poin;
        }

        protected  void onPreExecute(){
            super.onPreExecute();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username",username));
            params.add(new BasicNameValuePair("poin",poin));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);
                Log.d("URL : ", params.toString());

                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){
                            Log.d("Semua data: ", json.toString());
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){

        }
    }
}
