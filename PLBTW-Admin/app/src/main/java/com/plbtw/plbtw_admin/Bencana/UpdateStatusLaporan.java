package com.plbtw.plbtw_admin.Bencana;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.plbtw.plbtw_admin.CustomAdapter.ListBencanaAdapter;
import com.plbtw.plbtw_admin.JSONParser;
import com.plbtw.plbtw_admin.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kevin on 23/05/2016.
 */
public class UpdateStatusLaporan {

    private static final String URL_READ="http://pinjemindong.esy.es/plbtw/bencana/update_status.php";
    ArrayList<HashMap<String, String>> arrayListBencana;

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";


    public UpdateStatusLaporan(String id_laporan,String status){
        new UpdateStatusLaporanJSON(id_laporan,status).execute();
    }

    class UpdateStatusLaporanJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String status,id_laporan;

        public UpdateStatusLaporanJSON(String id_laporan,String status){
            this.status=status;
            this.id_laporan=id_laporan;
        }

        protected  void onPreExecute(){
            super.onPreExecute();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("status",status));
            params.add(new BasicNameValuePair("id_laporan",id_laporan));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);
                Log.d("URL : ", params.toString());

                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){
                            Log.d("Semua data: ", json.toString());
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){

        }
    }
}
