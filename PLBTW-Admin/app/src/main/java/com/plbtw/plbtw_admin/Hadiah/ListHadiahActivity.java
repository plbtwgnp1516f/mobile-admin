package com.plbtw.plbtw_admin.Hadiah;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.plbtw.plbtw_admin.CustomAdapter.ListHadiahAdapter;
import com.plbtw.plbtw_admin.JSONParser;
import com.plbtw.plbtw_admin.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kevin on 22/05/2016.
 */
public class ListHadiahActivity extends Fragment {

    private static final String URL_READ="http://pinjemindong.esy.es/plbtw/hadiah/read.php";
    ArrayList<HashMap<String, String>> arrayListHadiah;
    ListView listHadiah;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_list_hadiah, container, false);
        arrayListHadiah = new ArrayList<HashMap<String, String>>();
        listHadiah = (ListView) v.findViewById(R.id.listHadiah);


        Button btnTambah = (Button) v.findViewById(R.id.btnEdit);
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),TambahHadiahActivity.class));
            }
        });

        new listHadiahJSON().execute();
        return v;
    }

    //JSON kelas untuk login
    class listHadiahJSON extends AsyncTask<String, String, String> {
        String sukses = "";

        public listHadiahJSON(){
            pDialog = new ProgressDialog(getActivity());
        }

        protected  void onPreExecute(){
            super.onPreExecute();
            pDialog.setMessage("Mengambil Data. Silahkan Tunggu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);
                Log.d("URL : ", params.toString());

                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){

                            arrayListHadiah = new ArrayList<HashMap<String, String>>();
                            Log.d("Semua data: ", json.toString());

                            jsonarray = json.getJSONArray(TAG_HASIL);

                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);

                                String id_hadiah = jsonobject.getString("id_hadiah");
                                String nama_hadiah = jsonobject.getString("nama_hadiah");
                                String poin = jsonobject.getString("poin");
                                String jumlah = jsonobject.getString("jumlah");

                                //Log.d("",id_martabak+nama_martabak+harga+deskripsi+"\n");

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put("id_hadiah", id_hadiah);
                                map.put("nama_hadiah", nama_hadiah);
                                map.put("poin", poin);
                                map.put("jumlah", jumlah);

                                arrayListHadiah.add(map);
                            }
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")){
                ListAdapter adapter = new ListHadiahAdapter(getActivity(),arrayListHadiah,
                        R.layout.list_bencana,new String[]{},
                        new int[] {});
                listHadiah.setAdapter(adapter);
            }
            else
                Toast.makeText(getActivity().getApplicationContext(), "Error :(", Toast.LENGTH_SHORT).show();

        }
    }
}
