package com.plbtw.plbtw_admin.Hadiah;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.plbtw.plbtw_admin.JSONParser;
import com.plbtw.plbtw_admin.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class TambahHadiahActivity extends AppCompatActivity {
    private ImageView imageView;
    private Bitmap bitmap;
    private int PICK_IMAGE_REQUEST = 1;
    private String UPLOAD_IMAGE_URL ="http://pinjemindong.esy.es/plbtw/hadiah/upload.php";
    private String URL_CREATE ="http://pinjemindong.esy.es/plbtw/hadiah/create.php";
    private String KEY_IMAGE = "image";

    EditText txtNamaHadiah,txtJumlah,txtPoin;
    Button btnChose,btnTambah;

    ArrayList<HashMap<String, String>> arrayListBencana;
    ListView listBencana;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_hadiah);

        btnChose = (Button) findViewById(R.id.buttonChoose);
        btnTambah= (Button) findViewById(R.id.btnEdit);
        imageView  = (ImageView) findViewById(R.id.imageView);
        txtJumlah = (EditText) findViewById(R.id.txtJumlah);
        txtNamaHadiah = (EditText) findViewById(R.id.txtNamaHadiah);
        txtPoin = (EditText) findViewById(R.id.txtPoin);

        btnChose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkTextbox()) {
                    new TambahHadiahJSON(txtNamaHadiah.getText().toString(),txtPoin.getText().toString(),txtJumlah.getText().toString()).execute();
                    uploadImage();
                    clear();

                }
            }
        });
    }

    public void clear(){
        txtJumlah.setText("");
        txtPoin.setText("");
        txtNamaHadiah.setText("");
        imageView.setImageResource(0);
    }

    private boolean checkTextbox(){
        boolean temp=true;
        if(txtJumlah.getText().toString().isEmpty()){
            txtJumlah.setError("Jumlah tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtJumlah.setError(null);
        }
        if(txtNamaHadiah.getText().toString().isEmpty()){
            txtNamaHadiah.setError("Nama hadiah tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtNamaHadiah.setError(null);
        }
        if(txtPoin.getText().toString().isEmpty()){
            txtPoin.setError("Poin tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtPoin.setError(null);
        }

        return temp;
    }


    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void uploadImage(){
        //Showing the progress dialog

        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_IMAGE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();
                        //Showing toast message of the response
                        Toast.makeText(TambahHadiahActivity.this, "Gambar berhaisl diupload", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();

                        //Showing toast
                        Toast.makeText(TambahHadiahActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();
                //String username=sharedpreferences.getString("username","");

                //Adding parameters
                params.put(KEY_IMAGE, image);
                //params.put("username", username);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //JSON kelas untuk login
    class TambahHadiahJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String nama_hadiah,poin,jumlah;

        public TambahHadiahJSON(String nama_hadiah,String poin, String jumlah){
            this.nama_hadiah=nama_hadiah;
            this.poin=poin;
            this.jumlah=jumlah;
            pDialog = new ProgressDialog(TambahHadiahActivity.this);
        }

        protected  void onPreExecute(){
            super.onPreExecute();
            pDialog.setMessage("Mengambil Data. Silahkan Tunggu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("nama_hadiah",nama_hadiah));
            params.add(new BasicNameValuePair("poin",poin));
            params.add(new BasicNameValuePair("jumlah",jumlah));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_CREATE,"GET",params);
                Log.d("URL : ", params.toString());

                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){

                            arrayListBencana = new ArrayList<HashMap<String, String>>();
                            Log.d("Semua data: ", json.toString());

                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")) {
                clear();
                Toast.makeText(TambahHadiahActivity.this, "Data berhasil dimasukkan :D", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(TambahHadiahActivity.this, "Error :(", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
