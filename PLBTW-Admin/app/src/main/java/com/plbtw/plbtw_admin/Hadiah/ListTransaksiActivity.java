package com.plbtw.plbtw_admin.Hadiah;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.plbtw.plbtw_admin.CustomAdapter.ListTransaksiAdapter;
import com.plbtw.plbtw_admin.JSONParser;
import com.plbtw.plbtw_admin.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kevin on 29/05/2016.
 */
public class ListTransaksiActivity extends Fragment {

    private static final String URL_READ="http://pinjemindong.esy.es/plbtw/transaksi/read.php";
    ArrayList<HashMap<String, String>> arrayListTransaksi;
    ListView listTransaksi;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_list_transaksi, container, false);
        arrayListTransaksi = new ArrayList<HashMap<String, String>>();
        listTransaksi = (ListView) v.findViewById(R.id.listTransaksi);

        new listTransaksiJSON().execute();
        return v;
    }

    //JSON kelas untuk login
    class listTransaksiJSON extends AsyncTask<String, String, String> {
        String sukses = "",username;

        public listTransaksiJSON(){
            pDialog = new ProgressDialog(getActivity());
        }

        protected  void onPreExecute(){
            super.onPreExecute();
            pDialog.setMessage("Mengambil Data. Silahkan Tunggu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("status","belum diambil"));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);
                Log.d("URL : ", params.toString());

                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){

                            arrayListTransaksi = new ArrayList<HashMap<String, String>>();
                            Log.d("Semua data: ", json.toString());

                            jsonarray = json.getJSONArray(TAG_HASIL);

                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);

                                String id_transaksi = jsonobject.getString("id_transaksi");
                                String nama_hadiah = jsonobject.getString("nama_hadiah");
                                String username = jsonobject.getString("username");
                                String tanggal = jsonobject.getString("tanggal");
                                String status = jsonobject.getString("status");
                                String id_hadiah = jsonobject.getString("id_hadiah");

                                //Log.d("",id_martabak+nama_martabak+harga+deskripsi+"\n");

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put("id_transaksi", id_transaksi);
                                map.put("nama_hadiah", nama_hadiah);
                                map.put("id_hadiah", id_hadiah);
                                map.put("username", username);
                                map.put("tanggal", tanggal);
                                map.put("status", status);

                                arrayListTransaksi.add(map);
                            }
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")){
                ListAdapter adapter = new ListTransaksiAdapter(getActivity(),arrayListTransaksi,
                        R.layout.list_transaksi,new String[]{},
                        new int[]{});
                listTransaksi.setAdapter(adapter);
            }
            else
                Toast.makeText(getActivity().getApplicationContext(), "Error :(", Toast.LENGTH_SHORT).show();

        }
    }
}
