package com.plbtw.plbtw_admin.CustomAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.plbtw.plbtw_admin.Hadiah.EditHadiahActivity;
import com.plbtw.plbtw_admin.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Kevin on 23/05/2016.
 */
public class ListHadiahAdapter extends SimpleAdapter{

    private ArrayList<HashMap<String, String>> data;
    private Context context;
    private LayoutInflater inflater;

    public ListHadiahAdapter(Context context, ArrayList<HashMap<String, String>> data, int resource, String[] from, int[] to) {
        //super(context, data, resource, from, to);
        super(context, data, resource, from, to);
        Log.d("", "----------->Array " + data);
        this.data = data;
        this.context=context;
        //inflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public View getView(final int position, View vParentRow, ViewGroup parent) {
        View view = super.getView(position, vParentRow, parent);
        //View view=vParentRow;
        //if (view == null)
        {
            Log.d("","--------->POSITION: "+position);
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_hadiah,parent, false);

            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            //set Text
            final TextView lblNamaHadiah= (TextView) view.findViewById(R.id.lblNamaHadiah);
            TextView lblPoin= (TextView) view.findViewById(R.id.lblPoin);
            TextView lblIdHadiah = (TextView) view.findViewById(R.id.lblIdHadiah);
            TextView lblJumlah = (TextView) view.findViewById(R.id.lblJumlah);

            String url="http://pinjemindong.esy.es/plbtw/image/"+ data.get(position).get("id_hadiah")+".png";
            Log.d("", "--------->URL" + url);
            Picasso.with(context).load(url).into(imageView);

            if(imageView.getDrawable() == null){
                imageView.setImageResource(R.drawable.no_image);
            }

            lblNamaHadiah.setText(data.get(position).get("nama_hadiah"));
            lblPoin.setText("Poin :" + data.get(position).get("poin"));
            lblIdHadiah.setText("Jumlah :" + data.get(position).get("id_hadiah"));
            lblJumlah.setText("Jumlah :" + data.get(position).get("jumlah"));

            lblIdHadiah.setVisibility(View.GONE);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b=new Bundle();
                    b.putString("nama_hadiah",data.get(position).get("nama_hadiah"));
                    b.putString("jumlah",data.get(position).get("jumlah"));
                    b.putString("id_hadiah",data.get(position).get("id_hadiah"));
                    b.putString("poin",data.get(position).get("poin"));
                    Intent i = new Intent(context, EditHadiahActivity.class);
                    i.putExtras(b);
                    context.startActivity(i);
                }
            });
        }
        return view;
    }
}
