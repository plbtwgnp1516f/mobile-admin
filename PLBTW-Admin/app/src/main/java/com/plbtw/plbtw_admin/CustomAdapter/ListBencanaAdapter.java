package com.plbtw.plbtw_admin.CustomAdapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.plbtw.plbtw_admin.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Kevin on 18/05/2016.
 */
public class ListBencanaAdapter extends SimpleAdapter {
    private ArrayList<HashMap<String, String>> data;
    private Context context;
    private LayoutInflater inflater;

    public ListBencanaAdapter(Context context, ArrayList<HashMap<String, String>> data, int resource, String[] from, int[] to) {
        //super(context, data, resource, from, to);
        super(context, data, resource, from, to);
        Log.d("", "----------->Array " + data);
        this.data = data;
        this.context=context;
        //inflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public View getView(final int position, View vParentRow, ViewGroup parent) {
        View view = super.getView(position, vParentRow, parent);
        //View view=vParentRow;
        //if (view == null)
        {
            Log.d("","--------->POSITION: "+position);
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_bencana,parent, false);

            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

            String waktu =data.get(position).get("waktu");
            String hari = waktu.substring(0, 10);
            String jam = waktu.substring(11, 13);
            String menit = waktu.substring(14,16);
            String detik = waktu.substring(17,19);
            String username=data.get(position).get("username");

            String tanggal_final = username+"-"+hari+"-"+jam+"-"+menit+"-"+detik;
            String url="http://pinjemindong.esy.es/plbtw/upload_image/"+ tanggal_final+".png";

            Log.d("","--------->URL"+url);
            Picasso.with(context).load(url).into(imageView);

            if(imageView.getDrawable() == null){
                imageView.setImageResource(R.drawable.no_image);
            }

            //set Text
            TextView lblJenisBencana= (TextView) view.findViewById(R.id.txtJenisBencana);
            TextView lblUsername= (TextView) view.findViewById(R.id.txtUsername);
            TextView lblWaktu= (TextView) view.findViewById(R.id.txtWaktu);
            TextView lblKota = (TextView) view.findViewById(R.id.txtKota);
            TextView lblKecamatan = (TextView) view.findViewById(R.id.txtKecamatan);
            TextView lblKelurahan = (TextView) view.findViewById(R.id.txtKelurahan);
            TextView lblDesa = (TextView) view.findViewById(R.id.txtDesa);
            TextView lblAlamat = (TextView) view.findViewById(R.id.txtAlamat);
            TextView txtIdLaporan = (TextView) view.findViewById(R.id.txtIdLaporan);
            TextView txtKorban = (TextView) view.findViewById(R.id.txtKorban);
            TextView txtPenyebab = (TextView) view.findViewById(R.id.txtPenyebab);

            lblJenisBencana.setText(data.get(position).get("jenis_bencana")+"("+data.get(position).get("tingkat_kerusakan")+")");
            lblUsername.setText("Oleh :"+data.get(position).get("username"));
            lblWaktu.setText("Tanggal: "+data.get(position).get("waktu"));
            lblKota.setText("Kota: "+data.get(position).get("kota"));
            lblKecamatan.setText("Kecamatan: "+data.get(position).get("kecamatan"));
            lblKelurahan.setText("Kelurahan: "+data.get(position).get("kelurahan"));
            lblDesa.setText("Desa: "+data.get(position).get("desa"));
            lblAlamat.setText("Alamat: "+data.get(position).get("alamat"));
            txtIdLaporan.setText(data.get(position).get("id_laporan"));
            txtKorban.setText("Jumlah Korban: "+data.get(position).get("korban"));
            txtPenyebab.setText("Penyebab: "+data.get(position).get("penyebab"));
            txtIdLaporan.setVisibility(View.GONE);

            lblKecamatan.setVisibility(View.GONE);
            lblDesa.setVisibility(View.GONE);
            lblUsername.setVisibility(View.GONE);

        }
        return view;
    }
}
