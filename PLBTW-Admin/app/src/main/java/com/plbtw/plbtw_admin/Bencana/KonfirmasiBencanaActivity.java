package com.plbtw.plbtw_admin.Bencana;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.plbtw.plbtw_admin.CustomAdapter.KonfirmasiBencanaAdapter;
import com.plbtw.plbtw_admin.CustomAdapter.ListBencanaAdapter;
import com.plbtw.plbtw_admin.JSONParser;
import com.plbtw.plbtw_admin.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kevin on 22/05/2016.
 */
public class KonfirmasiBencanaActivity extends Fragment {

    private static final String URL_READ="http://pinjemindong.esy.es/plbtw/bencana/read.php";
    ArrayList<HashMap<String, String>> arrayListBencana;
    ListView listBencana;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_list_bencana, container, false);
        arrayListBencana = new ArrayList<HashMap<String, String>>();
        listBencana = (ListView) v.findViewById(R.id.listBencana);
        new listBencanaJSON("belum dicek").execute();

        return v;
    }

    //JSON kelas untuk login
    class listBencanaJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String status;

        public listBencanaJSON(String status){
            this.status=status;
            pDialog = new ProgressDialog(getActivity());
        }

        protected  void onPreExecute(){
            super.onPreExecute();
            pDialog.setMessage("Mengambil Data. Silahkan Tunggu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("status",status));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);
                Log.d("URL : ", params.toString());

                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){

                            arrayListBencana = new ArrayList<HashMap<String, String>>();
                            Log.d("Semua data: ", json.toString());

                            jsonarray = json.getJSONArray(TAG_HASIL);

                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);

                                String jenis_bencana = jsonobject.getString("jenis_bencana");
                                String username = jsonobject.getString("username");
                                String kota = jsonobject.getString("kota");
                                String kecamatan = jsonobject.getString("kecamatan");
                                String kelurahan = jsonobject.getString("kelurahan");
                                String desa = jsonobject.getString("desa");
                                String alamat = jsonobject.getString("alamat");
                                String waktu = jsonobject.getString("waktu");
                                String id_laporan = jsonobject.getString("id_laporan");
                                String penyebab = jsonobject.getString("penyebab");
                                String tingkat_kerusakan = jsonobject.getString("tingkat_kerusakan");
                                String korban = jsonobject.getString("korban");

                                //Log.d("",id_martabak+nama_martabak+harga+deskripsi+"\n");

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put("waktu", waktu);
                                map.put("jenis_bencana", jenis_bencana);
                                map.put("username", username);
                                map.put("kota", kota);
                                map.put("kecamatan",kecamatan);
                                map.put("kelurahan", kelurahan);
                                map.put("desa", desa);
                                map.put("alamat", alamat);
                                map.put("id_laporan", id_laporan);
                                map.put("penyebab", penyebab);
                                map.put("korban", korban);
                                map.put("tingkat_kerusakan", tingkat_kerusakan);

                                arrayListBencana.add(map);
                            }
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")){
                ListAdapter adapter = new KonfirmasiBencanaAdapter(getActivity(),arrayListBencana,
                        R.layout.list_bencana,new String[]{},
                        new int[] {});
                listBencana.setAdapter(adapter);
            }
            else
                Toast.makeText(getActivity().getApplicationContext(), "Error :(", Toast.LENGTH_SHORT).show();

            /*
            jumlah_data=listCart.getAdapter().getCount();
            if(jumlah_data==0){
                temp_view.setBackgroundResource(R.drawable.cart_empty);
                btnBayar.setVisibility(View.GONE);
            }*/
        }
    }
}
